#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
import copy
from trytond.model import ModelView, ModelSQL
from trytond.pool import Pool


class SaleLine(ModelSQL, ModelView):
    _name = 'sale.line'

    def get_invoice_line(self, line):
        pool = Pool()
        account_rule_obj = pool.get('account.account.rule')

        res, = super(SaleLine, self).get_invoice_line(line)
        tax_ids = [x.id for x in line.taxes]
        res['taxes'] = [('set', tax_ids)]
        res['account'] = account_rule_obj.get_account(
            'revenue', line.sale.party, line.sale.sale_date, line.product,
            tax_ids)
        return [res]

SaleLine()


class Template(ModelSQL, ModelView):
    _name = 'product.template'

    def __init__(self):
        super(Template, self).__init__()
        self.account_revenue = copy.copy(self.account_revenue)
        self.account_revenue.states = copy.copy(self.account_revenue.states)
        self.account_revenue.states['required'] = False
        self._reset_columns()

Template()
